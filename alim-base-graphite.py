#!/usr/bin/python
from openpyxl import load_workbook
from dateutil.parser import parse
import time, datetime, socket

CARBON_SERVER = '192.168.81.131' # modifier l'adresse IP du serveur
CARBON_PORT = 2003

sock = socket.socket()
sock.connect((CARBON_SERVER, CARBON_PORT))
        

wb2 = load_workbook('/home/thiebaut/hackaton/PTDH_Hackathon.xlsx')
print wb2.get_sheet_names()
sh1 = wb2['Pression temperature']

datemesure = sh1['A79711'].value
heuremesure = sh1['B79711'].value
dateheure = datemesure.strftime("%d/%m/%y") + ' ' + heuremesure.strftime("%H:%M:%S")
d = parse(dateheure)
intervallefin = time.mktime(d.timetuple()) # dernière date enregistrée

for num in range(2,79711):
    try:
        datemesure = sh1['A' + str(num)].value # date de mesure
        heuremesure = sh1['B' + str(num)].value # heure de mesure
        dateheure = datemesure.strftime("%d/%m/%y") + ' ' + heuremesure.strftime("%H:%M:%S")
        d = parse(dateheure)
        intervalle = time.time()+time.mktime(d.timetuple())-intervallefin
        
        temp = sh1['D' + str(num)].value # valeur de la température
        pression = sh1['E' + str(num)].value # valeur de la pression

        messaget = "temp.strasbourg %f %d\n" % (temp, int(intervalle))
        messagep = "pression.strasbourg %f %d\n" % (pression, int(intervalle))
        print messaget + messagep
        
        sock.sendall(messaget)
        sock.sendall(messagep)
        
    except Exception:
        pass

sock.close()
