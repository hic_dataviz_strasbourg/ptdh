Références et documentation :

Installation détaillée et utilisation de graphite : http://kaivanov.blogspot.fr/2012/02/how-to-install-and-use-graphite.html
https://graphite.readthedocs.org/en/latest/

Openpyxl : https://openpyxl.readthedocs.org/en/latest/

Ajout de données dans graphite : http://coreygoldberg.blogspot.fr/2012/04/python-getting-data-into-graphite-code.html

Déploiement de graphite : https://kevinmccarthy.org/2013/07/18/10-things-i-learned-deploying-graphite/

Migrer la base de configuration de graphite vers mysql : https://gist.github.com/toni-moreno/9779979

